# `dast-site-validation-test`

tiny web app that serves a text file's contents in the response body and
`Gitlab-On-Demand-DAST` header.

## useage

- drop file called `token.txt` in project root
- call `./dast-site-validation-test`
- copy `ngrok` url for validation
